# Dahua Camera Install

```sh
git clone https://gitlab.com/tyatinan/dahua-camera-install.git

cd dahua-camera-install

unzip install.zip

sudo chmod +x script.sh

sudo ./script.sh
```

Done. :3 

# ตั้งค่าความสว่างกล้องผ่าน โปรแกรม

```
sh cd /opt/DahuaTech/MVViewer/bin

./run.sh 
```
กดเชื่อมต่อกล้อง แล้วเลือกเครื่องหมายเพลย์ด้านล่างแบบ continuous กดเมนูสามขีดด้านบนเพื่อตั้งค่าความสว่างและสี
**ออกจากโปรแกรม ก่อนเข้า Dahua Camera

# แก้ปัญหา u3v_drv not exist

```sh 
cd /opt/DahuaTech/MVViewer/module/USBDriver/

sudo ./build.sh 

sh sudo ./loadDrv.sh
```
